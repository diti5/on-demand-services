/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.kader.service.demande.web.rest.vm;
