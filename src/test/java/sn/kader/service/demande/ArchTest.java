package sn.kader.service.demande;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.kader.service.demande");

        noClasses()
            .that()
            .resideInAnyPackage("sn.kader.service.demande.service..")
            .or()
            .resideInAnyPackage("sn.kader.service.demande.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..sn.kader.service.demande.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
